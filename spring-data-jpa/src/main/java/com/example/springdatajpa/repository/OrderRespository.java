package com.example.springdatajpa.repository;

import com.example.springdatajpa.entity.Order;
import org.aspectj.weaver.ast.Or;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;

public interface OrderRespository extends CrudRepository<Order, Long> {
    List<Order> findByUserId(Long userId);
    Page<Order> findAll(Pageable pageable);
    Page<Order> findAll();

    Optional<Order> findByIdAndUserId(Long id, Long userId);

}
