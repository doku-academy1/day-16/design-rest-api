package com.example.springdatajpa.controller;

import com.example.springdatajpa.entity.Order;
import com.example.springdatajpa.repository.UserRepository;
import com.example.springdatajpa.services.OrderServices;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.awt.print.Pageable;
import java.util.List;
import java.util.Map;

@RestController
public class OrderController {

    @Autowired
    UserRepository userRepository;
    @Autowired
    OrderServices orderServices;

//    @GetMapping("/orders")
//    public ResponseEntity<List<Order>> getAllOrder() {
//        return new ResponseEntity<>(orderServices.getAllOrders(), HttpStatus.OK);
//    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully get orders")
    })
    @GetMapping("/orders")
    public ResponseEntity<Map<String, Object>> getAllOrder(@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size) {
        return new ResponseEntity<>(orderServices.getAll(page, size), HttpStatus.OK);
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully get orders by user id")
    })
    @GetMapping("/users/{user_id}/orders")
    public ResponseEntity<List<Order>> getOrderById(@PathVariable Long user_id) {
        return new ResponseEntity<>(orderServices.getOrderByUserId(user_id), HttpStatus.OK);
    }

    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully create orders")
    })
    @PostMapping("/users/{user_id}/orders")
    public ResponseEntity<Order> addOrder(@PathVariable Long user_id, @RequestBody Order order) {
        orderServices.createOrder(user_id, order);
        return new ResponseEntity<>(order, HttpStatus.CREATED);
    }

    @ApiResponses(value = {
            @ApiResponse(code = 202, message = "Successfully edit order")
    })
    @PutMapping("/users/{user_id}/orders/{order_id}")
    public ResponseEntity<Order> updateOrder(@RequestBody Order order, @PathVariable Long user_id, @PathVariable Long order_id) {
        orderServices.updateOrderById(user_id, order_id, order);
        return new ResponseEntity<>(order, HttpStatus.ACCEPTED);
    }

    @ApiResponses(value = {
            @ApiResponse(code = 202, message = "Successfully delete order")
    })
    @DeleteMapping("/users/{user_id}/orders/{order_id}")
    public ResponseEntity<String> deleteOrder(@PathVariable Long user_id, @PathVariable Long order_id) {
        orderServices.deleteOrderById(user_id, order_id);
        return new ResponseEntity<>("Order deleted successfully!", HttpStatus.ACCEPTED);
    }
}
